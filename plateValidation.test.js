test("correctly detects if a plate is real or not", function (assert) {
  assert.equal(isValidPlate("6108DCT"), true, "real plate");
  assert.equal(
    isValidPlate("6108DCA"),
    false,
    "fake plate: letters contains a vowel"
  );
  assert.equal(
    isValidPlate("6108DOA"),
    false,
    "fake plate: letters contains vowels"
  );
  assert.equal(
    isValidPlate("6108DÑA"),
    false,
    "fake plate: letters contains an 'ñ' character"
  );
  assert.equal(
    isValidPlate("6108ÑÑA"),
    false,
    "fake plate: letters contains some 'ñ' characters"
  );
  assert.equal(
    isValidPlate("6108QCA"),
    false,
    "fake plate: letters contains a 'q' character"
  );
  assert.equal(
    isValidPlate("6108QCQ"),
    false,
    "fake plate: letters contains some 'q' characters"
  );
});

test("check that it resists the introduction of invalid characters", function (assert) {
  assert.equal(
    isValidPlate("23?8CTK"),
    false,
    "number contains an invalid character"
  );
  assert.equal(
    isValidPlate("2387TE&"),
    false,
    "letters contains an invalid character"
  );
  assert.equal(
    isValidPlate("2$?8CTK"),
    false,
    "number contains some invalid characters"
  );
  assert.equal(
    isValidPlate("2387%E&"),
    false,
    "letters contains some invalid characters"
  );
  assert.equal(
    isValidPlate("2#14(AB"),
    false,
    "plate contains invalid characters"
  );
});

test("check that it resists a change of order (i.e. swap number with letters)", function (assert) {
  assert.equal(isValidPlate("6108A3C"), false, "letters contains a number");
  assert.equal(isValidPlate("6108A34"), false, "letters contains some numbers");
  assert.equal(isValidPlate("6T08DCT"), false, "number contains a character");
  assert.equal(
    isValidPlate("6T0BDCT"),
    false,
    "number contains some characters"
  );
  assert.equal(isValidPlate("DCTB853"), false, "full swap number with letters");
});

test("check that it resists if the number of digits is different from the expected standard", function (assert) {
  assert.equal(
    isValidPlate("681DCT"),
    false,
    "number of less than the standard"
  );
  assert.equal(
    isValidPlate("6108DT"),
    false,
    "letters of fewer characters than the standard"
  );
  assert.equal(
    isValidPlate("681DT"),
    false,
    "plate of fewer digits than the standard"
  );
});

test("check that it resists if no plate is entered", function (assert) {
  assert.equal(isValidPlate(""), false, "empty plate");
});
