/*
 * Given the three sides of a triangle it calculates the type of the
 * triangle.
 */
function isValidPlate(plate) { // eslint-disable-line no-unused-vars
  var re = /(\d\d\d\d[BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ][BCDFGHJKLMNPRSTVWXYZ])/i;
  if (plate.match(re) != null) {
    gtag('event', 'click', { // eslint-disable-line
      'event_category': 'validate',
      'event_label': plate.match(re)
    });
    return true;
  } else {
    return false;
  }
}
